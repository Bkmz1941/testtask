@extends('layout')
    <nav class="navbar bg-faded">
        <a class="navbar-brand" href="{{ route('home') }}">На главную</a>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="btn-group-vertical">
                    <button type="button" onclick='circle()' class="btn btn-primary">Круг</button>
                    <button type="button" onclick='square()' class="btn btn-primary">Квадрат</button>
                    <button type="button" onclick='triangle()' class="btn btn-primary">Треугольник</button>
                </div>
            </div>
            <div id='dashbord' class="col-lg-10">
                <div id='red'>
                    <center>
                        <h4>Выберите фигуру для отображения</h4>
                    </center>
                </div>
            </div>
        </div>
    </div>
<script>
    function circle() {
        $( "#red" ).remove();
        $( "#1" ).remove();
        $( "#2" ).remove();
        $( "#3" ).remove();
        $('#dashbord').append('<div id=\'1\'><center><div class=\"circle\"></div><center></div>');
    }
    function square() {
        $( "#red" ).remove();
        $( "#1" ).remove();
        $( "#2" ).remove();
        $( "#3" ).remove();
        $('#dashbord').append('<div id=\'2\'><center><div class=\"square\"></div><center></div>');
    }
    function triangle() {
        $( "#red" ).remove();
        $( "#1" ).remove();
        $( "#2" ).remove();
        $( "#3" ).remove();
        $('#dashbord').append('<div id=\'3\'><center><div class=\"triangle-up\"></div><center></div>');
    }
</script>